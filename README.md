## Bot Twitter compteur de jours

#### Introduction
Si vous me suivez sur Twitter, vous avez sûrement vu mes tweets du "bot" qui tweete le nombre de jours depuis ma dernière pizza.
![Screen d'un de mes tweets crées avec ce bot - 132 jours depuis ma dernière pizza - depuis le 3 mars 2021 (via mon bot Pizza).](images/screenTwitter.jpg)

Le "bot" en question, c'est juste un script Python auquel je donne une date, le tweet que je veux poster, calcule le nombre de jours et poste le tweet.
Dans ce guide, je vais vous décrire comment faire pour tout installer, paramétrer le script et faire en sorte qu'il se lance chaque jour.

#### Installation

##### Instalation de Python
Tout d'abord il vous faut la dernière version de Python, téléchargeable [ici](https://www.python.org/downloads/).
Installez-la et ensuite ouvrez un Terminal.

- Sous Windows: appuyez sur Windows et R en même temps,ez cmd puis appuyez sur Ok.
![Lancement du Terminal sous Windows](images/cmd.jpg)
- Sous Mac: cliquez sur la petite loupe en haut de l'écran et tapez "Terminal"
<p align="left">
  <img style="max-height:400px" src="images/terminal-mac.jpg" alt="Lancement du Terminal sous Mac"/>
</p>

Une fois le Terminal ouvert, tapez `python --version` puis appuyez sur Entrée.
Normalement (si Python est bien installé), la commande affiche la version de Python actuellement installée (ne vous inquiétez pas si vous n'avez pas la même version que celle sur ma capture d'écran - il va sûrement y avoir une version plus récente que celle qui tourne sur ma machine à l'heure où j'écris ces lignes).

Si la commande renvoie une erreur, c'est sûrement que Python n'est pas ou mal installé donc essayez de le ré-installer.
![Affichage de la version de Python dans le Terminal](images/pythonVersion.jpg)

Maintenant que Python est bien installé, il faut maintenant installer le module Tweepy - qui comme son nom l'indique partiellement, sert à gérer toute la partie Twitter.
Pour ce faire, entrez la commande `python -m pip install tweepy`
Si tout se passe bien, vous devriez avoir un message `Successfully installed tweepy`

##### Rejoindre le programme "Twitter pour développeur.euse.s" et créer une application Twitter

Pour que le script puisse poster un tweet, il aura besoin de quatre clés. Pour faire très simple, voyez ça comme 4 longs mots de passe que le bot envoie à Twitter pour bien dire qu'il a l'autorisation de poster sur votre compte.

Pour avoir ces clés, il faut tout d'abord rejoindre le programme "Twitter pour développeur.euse.s".
Le processus d'inscription est en anglais et est plutôt long mais je vais essayer de bien tout décrire.

1. Tout commence [ici](https://developer.twitter.com/en/apply-for-access)

  Sur la page d'accueil, cliquer sur *Apply for a developer account*.
![Page d'accueil](images/twitter-dev-01.png)

2. Ici, on vous demande pourquoi vous voulez avoir accès à un compte développeur. Cochez *Hobbyist* puis *Making a bot*
![Page d'accueil](images/twitter-dev-02.png)

Plus bas:
- *How would you lokeus to call you ?* Entrez votre nom (ou un pseudo, ça n'a pas vraiment d'importance)
- *What country do you live in ?* Le pays où vous habitez.
- *Current coding skill level* Votre niveau de connaissance en programmation (là aussi aucune importance)
- *Twitter API news* décocher la case pour préserver votre boîte mail de mails inutiles :)
![Page d'accueil](images/twitter-dev-03.png)

3. Sur la page suivante, on vous demande de décrire pourquoi vous voulez avoir accès à un compte développeur. Il faut écrire un texte d'au moins 200 caractères donc dans notre cas, on va juste dire qu'on a envie de poster un tweet quotidien avec le nombre de jours depuis un événement particulier.
Vous pouvez copier-coller le texte suivant:
```
Post a daily scheduled tweet with the number of days since a particular event.
Filling the text to reach the 200 characters limit because I have nothing else to say.
Filling the text to reach the 200 characters limit because I have nothing else to say.
```
![Page d'accueil](images/twitter-dev-04.png)

Plus bas, on vous demande quelles fonctionnalités vous comptez utiliser ?
- *Are you planning toanalyze Twitter data ?* Non
- *Will your app use Tweet, Retweet, Like, Follow, or Direct Message functionnality ?* Oui 
- Comme tout à l'heure, on vous demande en quoi vous allez utiliser la fonctionnalité de tweet, vous pouvez copier-coller le même texte que tout à l'heure en remplaçant le 200 par un 100 :D
```
Post a daily scheduled tweet with the number of days since a particular event.
Filling the text to reach the 100 characters limit because I have nothing else to say.
```   
![Page d'accueil](images/twitter-dev-05.png)

- *Do you plan to display Tweets or aggregate data about Twitter content outside Twitter ?* Non.
- *Will your product, service, or analysis make Twitter content or derived information available to a government entity ?* Non.
![Page d'accueil](images/twitter-dev-06.png)

Sur la page qui suit, vous avez un résumé de toutes les informations que vous avez entré - faites attention à ce que l'adresse mail soit valide et que vous y avez accès parce que vous devrez confirmer votre inscription.
![Page d'accueil](images/twitter-dev-07.png)

Ensuite, vous avez des termes à accepter.
![Page d'accueil](images/twitter-dev-08.png)

Et enfin, vous avez la confirmation que votre inscription s'est bien passée.

![Page d'accueil](images/twitter-dev-09.png)

Dernière étape: vous avez reçu un mail où vous devez cliquer sur un lien pour confirmer votre inscription.