from datetime import date
import sys
import tweepy
import time
import argparse
from json import load
import re

moisTab = ["janvier", "février", "mars", "avril", "mai", "juin",
           "juillet", "août", "septembre", "octobre", "novembre", "décembre"]


with open("parametres.json") as f:
    data = load(f)

consumer_key = data["consumerKey"]
consumer_secret = data["consumerSecret"]
access_token = data["accessToken"]
access_token_secret = data["accessTokenSecret"]
message = data["message"]
dateStr = data["date"]


def test():
    print("Tweepy correctement installé !")
    print("Options actuelles: ")
    print(f'\tMessage: "{message}"')
    print(f'\tDate de référence: {dateStr}')

    if (getDateReference()):
        print("\tDate correcte")

    return


def getDateReference():
    datePattern = r"(\d{2})\/(\d{2})\/(\d{4})"
    result = re.search(datePattern, dateStr)

    try:
        if result:
            jour = int(result.group(1))
            mois = int(result.group(2))
            annee = int(result.group(3))
            d = date(annee, mois, jour)

        else:
            raise ValueError

    except ValueError:
        print(
            f'Erreur: {dateStr} est une date incorrecte.\nElle doit être au format JJ/MM/AAAA', file=sys.stderr)
        sys.exit(1)

    return d


def trierDates(date1, date2):
    depart = date1 if (date1 < date2) else date2
    fin = date2 if (depart == date1) else date1
    return depart, fin


def nombreJours(date1, date2):
    depart, fin = trierDates(date1, date2)
    return (fin - depart).days


def dateToString(d):
    return f'{d.day} {moisTab[d.month - 1]} {d.year}'


def createTweet(nbJours, dateReference):
    tweet = f'{nbJours} jours depuis {message} - depuis le {dateToString(dateReference)} (via mon bot).'

    if (nbJours < 2):
        tweet = tweet.replace("jours", "jour")

    return tweet


def postTweet(tweet):
    api = None
    nbErreurs = 0
    nbErreursMax = 5

    try:
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        api = tweepy.API(auth)
        print("Connexion à Twitter réussie")

    except Exception:
        nbErreurs += 1
        print(
            f"Échec de la connexion à Twitter -- Autre essai dans 5 secondes - Erreur {nbErreurs}/{nbErreursMax}", file=sys.stderr)
        time.sleep(5)

        if (nbErreurs == nbErreursMax):
            print(
                "Nombre d'erreurs maximal atteint - sortie du programme", file=sys.stderr)
            sys.exit(1)

    nbErreurs = 0
    print(tweet)

    try:
        api.update_status(tweet)
        print("Tweet envoyé")

    except Exception:
        nbErreurs += 1
        print(
            f"Échec lors de l'envoi du tweet - Erreur {nbErreurs}/{nbErreursMax}", file=sys.stderr)
        if (nbErreurs == nbErreursMax):
            print("Nombre d'erreurs maximal atteint - sortie du programme",
                  file=sys.stderr)
            sys.exit(1)

    return


if __name__ == '__main__':
    dateReference = getDateReference()
    nbJours = nombreJours(dateReference, date.today())
    tweet = createTweet(nbJours, dateReference)
    print(tweet)
    # postTweet(tweet)
